#include <iostream>

#include "mongoose.h"


using namespace std;


struct mg_mgr mgr;


static void cb (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	auto url = const_cast <const char *> (reinterpret_cast <char *> (fn_data));

	if (ev == MG_EV_CONNECT) {
		struct mg_str host = mg_url_host (url);
		if (mg_url_is_ssl (url)) {
			struct mg_tls_opts opts = {
				.ca = "cacert.pem",
			};
			mg_tls_init (c, &opts);
		}
		mg_printf (
			c,
			"GET %s HTTP/1.0\r\nHost: %.*s\r\n\r\n",
			mg_url_uri (url),
			(int) host.len,
			host.ptr
		);
	} else if (ev == MG_EV_HTTP_MSG) {
		cerr << "<<OK>> " << url << endl;
		struct mg_http_message *hm = (struct mg_http_message *) ev_data;
		printf (
			"%.*s",
			(int) hm->body.len,
			hm->body.ptr
		);
		c->is_closing = 1;
	}
}


static void get (const char * url)
{
	cerr << "TRY " << url << endl;
	mg_http_connect (&mgr, url, cb, const_cast <char *> (url));
}


int main (int argc, char *argv[]) {
	mg_mgr_init (&mgr);

	// get ("https://www.howsmyssl.com/");
	get ("https://clienttest.ssllabs.com:8443/ssltest/viewMyClient.html");
	
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
	}
	mg_mgr_free(&mgr);
	return 0;
}

