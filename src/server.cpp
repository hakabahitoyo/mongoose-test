#include "mongoose.h"


using namespace std;


static const char *s_listening_address = "http://localhost:8000";


static void cb (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	if (ev == MG_EV_HTTP_MSG) {
		mg_http_reply (
			c,
			200,
			"Content-Type: text/plain\r\n",
			"%s",
			"This is a pen."
		);
	}
}


int main(int argc, char *argv[]) {
	struct mg_mgr mgr;
	mg_mgr_init (&mgr);
	mg_http_listen (&mgr, s_listening_address, cb, &mgr);
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
	}
	mg_mgr_free(&mgr);
	return 0;
}

